[back to main](README.md)

Sources: [archlinux.org/REFInd](https://wiki.archlinux.org/index.php/REFInd)

# Edit `refind.conf`

Edit `ESP/refind.conf` (`ESP` is [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition)) and add IP address settings.

You can either rely on DHCP server on you network:
```
ip=:::::eth0:dhcp
```
or you can hardcode the IP address:
```
ip=192.168.1.100:::::eth0:none
```

The example of `ESP/refind.conf`:
```
"Boot with standard options"  "cryptdevice=/dev/nvme0n1p2:system_luks root=/dev/system_vg/root ip=:::::eth0:dhcp rw add_efi_memmap initrd=initramfs-%v.img"
```


