[back to main](README.md)

Sources: [archlinux.org/GRUB](https://wiki.archlinux.org/index.php/GRUB)

# Edit `/etc/default/grub`

Edit `/etc/default/grub` and add IP address settings.

You can either rely on DHCP server on you network:
```
ip=:::::eth0:dhcp
```
or you can hardcode the IP address:
```
ip=192.168.1.100:::::eth0:none
```

The example of `/etc/default/grub`:
```
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet cryptdevice=/dev/nvme0n1p2:system_luks ip=:::::eth0:dhcp"
```


