Sources: [archlinux.org/Dm-crypt](https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#Remote_unlocking_of_the_root_(or_other)_partition)


# How to remotely unlock dm-crypt device

Guide to remotely (via `ssh`) unlock system disk encrypted with `dm-crypt` on `ArchLinux`.

The booting system will set networking card and start ssh server. You will be able to unlock the encrypted system via ssh, but also with attached keyboard (as usual).


## Pre-requisites

- At least two computers on the same nework
	- Computer to be remotelly unclocked - codename `mars_rover`
	- Computer that you will use for remote unlocking - codename `mission_control`
- Expectations for `mars_rover`
	- Installed system (`ArchLinux`)
	- System disk is encrypted with `dm-crypt` (except `/boot`).
	- Static IP address (`192.168.1.100` is used in this guide as example)
- Expectations for `mission_control`
	- ssh client compatible with `Ed25519` and `ECDSA` key types


# Guide


## Install packages

Install required packages to `mars_rover`:
```
pacman -S --needed mkinitcpio-netconf mkinitcpio-utils mkinitcpio-tinyssh
```


## Setup SSH keys

On `mission_control` computer, generate `ed25519` key pair for ssh ([example of ssh-keygen](ssh-keygen.md)). Copy the public key (`*.pub`) into `mars_rover` to file called `/etc/tinyssh/root_key`.

Example content of `/etc/tinyssh/root_key`:
```
ssh-ed25519 AAAAC3N..... whatever_comment_you_chose
```

This file can contain multiple keys. Basically, on `mars_rover` you can copy the `~/.ssh/authorized_keys` to `/etc/tinyssh/root_key` and it will work.
```
cp /home/<user>/.ssh/authorized_keys /etc/tinyssh/root_key
```

**NOTE:** *Every-time you make changes to these keys, you have to regenerate `initramfs` by running `mkinitcpio -P`.*


## Edit mkinitcpio.conf

Edit hooks in `/etc/mkinitcpio.conf` in `mars_rover` by adding `netconf tinyssh encryptssh`
```
...
HOOKS=(base udev autodetect modconf block netconf tinyssh encryptssh lvm2 filesystems keyboard fsck)
...
```

**NOTE:** *(`encryptssh` replaces the `encrypt`)*


## Edit bootloader settings

Edit bootloader setting in `mars_rover` by adding IP address.

You can either rely on DHCP server on you network:
```
ip=dhcp
```

**NOTE:** *Use DHCP, since hardcoding IP address might be broken. However, if there will be no DHCP server on network, the boot will hang and wait for DHCP (at that moment direct manual entry will not be possible).*

For more specific instructions see [example of rEFInd configuration](refind.md) and [example of GRUB2 configuration](grub.md).


## Regenerate initramfs

Regenerate `initramfs` in `mars_rover`:

```
mkinitcpio -P
```


## Reboot the computer and ssh into it

Reboot `mars_rover`. If everything went according to plan, then the screen should promt you for password to decrypt the disk as usual, but there should be also mention of ssh server running (for example `Starting tinyssh`).

Now it should be possible to connect to `mars_rover` from `mission_control` with:
```
ssh root@192.168.1.100
```
Keep in mind that for unlocking the `dm-crypt`, you have to use `root` user (this is not real root - this root is only in the `initrd`).


# Troubleshooting

## WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!

### Cause
This happens because your `mission_control` already has the computer in list `~/.ssh/known_hosts` and the fingerprint does not match.
This is expected since your ordinary ssh server is independent (and different) from the `tinyssh` server running from `initramfs`.

### Solution
On the `mission_control`, go to `~/.ssh/known_hosts` and comment out with `#` (hash/number sign) character the fingerprint for the `mars_rover`. Connect to `mars_rover`, confirm to add fingerprint and unlock the sysntem disk.

Then edit the `~/.ssh/known_hosts` in `mission_control` again (now with the new fingerprint) and simply uncomment the previous fingerprint - such that there will be 2 fingerprints for the `mars_rover`.

## Error while decrypting: `libgcc_s.so.1 must be installed for pthread_cancel to work`

### Solution
Add `/usr/lib/libgcc_s.so.1` to the [BINARIES](https://wiki.archlinux.org/index.php/Mkinitcpio#BINARIES_and_FILES) array.

## Unable to negotiate with XXX.XXX.XXX.XXX port 22: no matching host key type found. Their offer:

### Cause
This happens because the `tinyssh` server has no server key.

### Solution
Generate a set of server keys using `openssh`, then convert it to `tinyssh` format a place it in `/etc/tinyssh/sshkeydir/`.

```
# mkdir /etc/tinyssh/openssh_keys
# ssh-keygen -t ed25519 -f /etc/tinyssh/openssh_keys/${HOSTNAME}.key -C "${HOSTNAME} tinyssh key"
# tinyssh-convert /etc/tinyssh/sshkeydir < /etc/tinyssh/openssh_keys/${HOSTNAME}.key
```

If you get error: `tinyssh-convert: fatal: out-tinysshkeydir exist`, simply remove the output directory with `rmdir /etc/tinyssh/sshkeydir`.

