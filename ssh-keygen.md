[back to main](README.md)

# ssh-keygen

`tinySSH` supports `Ed25519` and `ECDSA` key types. To select which you can read more about it at [archlinux.org/SSH_keys/Choosing_the_authentication_key_type](https://wiki.archlinux.org/index.php/SSH_keys#Choosing_the_authentication_key_type).

`Ed25519` is faster and more secure than `ECDSA`, therefore it is used in example below.

## ssh-keygen ed25519

Generate `Ed25519` kay pair:
```
ssh-keygen -t ed25519 -f ~/.ssh/whatever_name_you_chose.key -C "whatever_comment_you_chose"
```

Result should be key pair:
```
~/.ssh/
	|-- whatever_name_you_chose.key
	|-- whatever_name_you_chose.key.pub
```


# Sources

[archlinux.org/SSH_keys](https://wiki.archlinux.org/index.php/SSH_keys#Generating_an_SSH_key_pair)

